package bank.deposits;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class ClientReader {

    public static List<Client> parseClientXML(String fileName) {
        List<Client> clientList = new ArrayList<>();

        Client client = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));

            while (xmlEventReader.hasNext()){

                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();

                    if(startElement.getName().getLocalPart().equals("client")){
                        client = new Client();
                    }

                    if(startElement.getName().getLocalPart().equals("firstname")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setFirstName(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("lastname")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setLastName(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("deposit")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setDepositAmount(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();

                    if(endElement.getName().getLocalPart().equals("client")){
                        clientList.add(client);
                    }
                }
            }
        } catch (FileNotFoundException e) { e.printStackTrace(); }
          catch (XMLStreamException e) { e.printStackTrace(); }

        return clientList;
    }
}