package bank.deposits;

import java.util.List;

public class Application {
    public static void main(String[] args){

        List<Client> clList = ClientReader.parseClientXML("src/bank/deposits/clientsData.xml");

//        CountDeposits.meanNumber(clList);
//        CountDeposits.tenBigDeposits(clList);
        CountDeposits.bigDepositsWithoutRepeated(clList, 10);
    }
}
