package bank.deposits;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CountDeposits {

    public static void meanNumber(List<Client> clientList){
        double meanSumm = 0;

        for (int i = 0; i < clientList.size(); i++) {
            Client client = clientList.get(i);
            meanSumm += client.getDepositAmount();
        }

        //BigDecimal round method for financial operations
        double num = meanSumm / clientList.size();
        double meanNumber = new BigDecimal(num).setScale(3, RoundingMode.UP).doubleValue();

        System.out.println("Mean number of deposits: " + meanNumber);
    }

    public static List<Client> sortClientList(List<Client> clientList){
        for (int i = 0; i < clientList.size() - 1 ; i++) {
            Client clientI = clientList.get(i);

            for (int j = i + 1; j < clientList.size(); j++) {
                Client clientJ = clientList.get(j);

                if(clientI.getDepositAmount() < clientJ.getDepositAmount()){
                    Client buffClient = clientList.get(i);
                    clientList.set(i, clientList.get(j));
                    clientList.set(j, buffClient);
                }
            }
        }

        return clientList;
    }

    //10 first big deposits
    public static void tenBigDeposits(List<Client> clientList){

        sortClientList(clientList);

        for (int i = 0; i < clientList.size() && i < 10 ; i++) {

            String name = clientList.get(i).getFirstName();
            String lastName = clientList.get(i).getLastName();
            double depositNumber = clientList.get(i).getDepositAmount();

            System.out.println(name + " " + lastName + " has biggest deposit " + depositNumber);
        }
    }

    //count = number of showing deposits whithout repeated
    public static void bigDepositsWithoutRepeated(List<Client> clientList, int count){

        sortClientList(clientList);

        for (int i = 0; i < clientList.size() - 1 && i < count ; i++) {

            String name = clientList.get(i).getFirstName();
            String lastName = clientList.get(i).getLastName();
            double depositNumber = clientList.get(i).getDepositAmount();

            if(i == 0) {
                System.out.println(name + " " + lastName + " has biggest deposit " + depositNumber);
            }

            if(i > 0 && clientList.get(i).getDepositAmount() != clientList.get(i-1).getDepositAmount()){
                System.out.println(name + " " + lastName + " has biggest deposit " + depositNumber);
            } else {
                count++;
            }
        }
    }
}